# Connecting to Kubernetes #

In order to complete this lab exercise you need Kubernetes commandline tool called kubectl. Kubectl acts as Kubernetes client and it is used to run commands against Kubernetes clusters running on remote servers.  While you have the option of installing kubectl on your laptop and using it for performing the exercise we recommend using Google Cloud Shell.

In order for the kubectl to connect to the appropriate Kubernetes clusters it needs a kubeconfig file that has the required configuration details like the IP address of the cluster and log-in credentials. We have setup Kubernetes Cluster exclusive for this masterclass and have generated unique kubeconfig file for each of the participants and sent them individually to the registered email address. Please check for email from 'CNA Certification Team' and download the kubeconfig file and have it ready for the exercise.

## `kubectl` Configuration ##

1. Type the below command at the shell prompt. This command lists the kubernetes client version and the Kubernetes cluster server version if there is one.

    ``` sh
    kubectl version
    ```

    You will see output as something like below. It displays kubectl version and below it you can notice message of connection refused. It indicates that there is not Kubernetes Cluster server

    ``` text
    Client Version: version.Info{Major:"1", Minor:"12+", GitVersion:"..", GitCommit:"….", GitTreeState:"clean", BuildDate:"2019-06-24T19:27:39Z", GoVersion:"go1.10.8b4", Compiler:"gc", Platform:"linux/amd64"}
    The connection to the server localhost:8080 was refused - did you specify the right host or port?
    ```

2. In order to connect to the Kubernetes Cluster we need to use the kubernetes configuration file that was sent through email as mentioned earlier in the pre-requisite. Store the file sent through email to a local directory in laptop.

3. Rename the file to 'config' without any extension.

4. Upload the 'config' file to Google Cloud shell by selecting the upload icon indicated by three dots in the top right corner of the Shell window. The file will be uploaded to your home directory which is the default directory when you log into the Shell. (/home/&lt;google userid&gt;)

   ![Upload](img/upload-file.png)

5. Check if the file is uploaded properly by issuing 'ls' command and verify if the file by name 'config' is present in the home directory. If not found please repeat the earlier step to upload.

6. The file has to be moved to a directory called '.kube' under the home directory where kubectl refers by default. Hence create a directory called '.kube' in your home directory by issuing the below command. Watch out for the '.' in front of the word kube

    ``` sh
    mkdir .kube
    ```

7. Move the config file to the '.kube' folder

    ``` sh
    mv config .kube
    ```

8. Run the below command to check if kubectl is able to connect to the Kubernetes Cluster.

    ``` sh
    kubectl version
    ```

     You will see output like below. Both Kubernetes Client and Server version will be displayed.

    ``` text
    Client Version: version.Info{Major:"1", Minor:"12+", ...
    Server Version: version.Info{Major:"1", Minor:"14+", ...
    ```

Congratulations. You are now connected to the Kubernetes Cluster and ready to perform the below exercises
