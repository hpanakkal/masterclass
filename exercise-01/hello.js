var http = require('http'); //create a http server object:

var port = process.env.PORT || 8080;

http.createServer(function (req, res) {
  res.write('Hello World!'); //write a response to the client
  res.end(); //end the response
}).listen(port, (err) => {
  if ( err ) throw err;
  console.log('Server running at ' + port); // Print in console when server starts
}); //the server object listens on port 8080
