# Exercise 4 #

## Add product listing UI ##

In this exercise we will create a simple API endpoint and access it from a simple UI

1. Expose the webserver deployment as a service

    ```sh
    kubectl create configmap store \
        --from-file=exercise-04/ui
    ```

2. Update the deployment to use the new config map

    ```sh
    kubectl apply -f exercise-04/webserver.yml
    ```

## Add API deployment & service ##

1. Create a new config map for the API

    ``` sh
    kubectl create configmap products --from-file=db.json=exercise-04/api/products.json
    ```

2. Create the service for the API

    ```sh
    kubectl create -f exercise-04/service.yml
    ```

3. Create the deployment for the API

    ```sh
    kubectl create -f exercise-04/api.yml
    ```

## Expose UI & API via Ingress ##

4. Update the existing ingress with path based routing. Make sure to replace the `<namespace>` with your namespace so, that it does not clash with other participants

    ```sh
    kubectl apply -f exercise-04/ingress.yml
    ```

5. Access the webserver from [&lt;namespace&gt;.cna.wcloudacademy.com/ui](http://cna.wcloudacademy.com/ui)

   And you should be greeted with a product listing page displayed below

   ![Preview Products](/img/preview-products.png)

## Kubernetes Resources ##

![Exercise 03](img/ex-04.png)
