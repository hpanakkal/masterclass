# Exercise 2 #

## Deploy a webserver ##

In this exercise we will deploy a simple webserver on to the kubernetes cluster and customizing the output without modifiying the container image

1. Create a web server ([Nginx](https://www.nginx.com/)) deployment based on the [nginx](https://hub.docker.com/_/nginx/) docker image

    ```sh
    kubectl create deploy webserver --image=nginx
    ```

    Alternately use the [webserver.yml](exercise-02/webserver.yml)

    ```sh
    kubectl apply -f exercise-02/webserver.yml
    ```

2. Access the deployment by using a port forward

    ```sh
    kubectl port-forward deploy/webserver 8080:80
    ```

3. Access the web server using the web preview feature of the Google Cloud Shell

    ![Web Preview](img/web-preview.png)

4. You should be able to access the standard nginx page

    ![Default Nginx](img/preview-nginx.png)


## Change content served by webserver   ##

1. We will now customize this page. Create a `sample.html` file

    ```sh
    cat > sample.html
    <!doctype html>
    <html lang="en">
      <head>
        <meta charset="UTF-8"/>
        <title>Hello World!</title>
      </head>
      <body>
        <h1>Hello World!</h1>
      </body>
    </html>
    Ctrl+D
    ```

    You can also use the cloud shell editor to customize the html content

2. Create a config map with the contents of the file

    ```sh
    kubectl create configmap index --from-file=index.html=sample.html
    ```

3. Export the deployment yaml

    ``` sh
    kubectl get deploy/webserver -o yaml > webserver.yaml
    ```

4. Edit the `webserver.yaml` to attach the config map created in *step 6*

    ``` yaml
    apiVersion: extensions/v1beta1
    kind: Deployment
      #...
    spec:
      replicas: 1
      #...
      template:
        #...
        spec:
          containers:
          - image: nginx
            name: nginx
            volumeMounts:
            - name: index
              mountPath: /usr/share/nginx/html/index.html
              subPath: index.html
          volumes:
          - name: index
            configMap:
              name: index
    ```

    Alternately use the [webserver-cm.yml](exercise-02/webserver-cm.yml)

    ```sh
    kubectl apply -f exercise-02/webserver-cm.yml
    ```

5. Notice the `replicas` key which specifies the number of instances of the pod to run we can easily scale up/down the deployment by giving the desired number of replicas.

    ``` sh
    kubectl scale --replicas=2 deploy/webserver
    ```

6. Check the number of pods running

    ``` sh
    kubectl get pods
    ```

7. Scale down the deployment to a single replica

    ``` sh
    kubectl scale --replicas=1 deploy/webserver
    ```

    Notice how the pods are automatically terminated and removed

5. Expose deployment using a port forward

    ```sh
    kubectl port-forward deploy/webserver 8080:80
    ```

6. Use the web preview feature to check the update

    ![Sample Preview](img/preview-sample.png)

## Kubernetes Resources ##

![Exercise 02](img/ex-02.png)
