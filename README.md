# CNA Masterclass #

Instructions and setup for CNA Masterclass

## Pre-requisites ##

1. Internet capable laptop/desktop
2. Internet connectivity
3. Google account to access [Google Cloud Shell](https://console.cloud.google.com/cloudshell/editor?shellonly=true)
4. A wipro email to receive the configuration file to connect to the kubernetes cluster

### Logging into Google Cloud Shell ###

1. Open [https://console.cloud.google.com/cloudshell/editor?shellonly=true](https://console.cloud.google.com/cloudshell/editor?shellonly=true) in your browser
2. You need to log in to your Google account.
3. If the shell prompt does not open automatically, then explicitly select the icon that will open Cloud Shell prompt

   ![Prompt](/img/shell-prompt.png)

    You can make it to appear in full screen by selecting the 'Computer' icon to the left of the circled shell icon.

**NOTE**: As Cloud Shell is a free service there is weekly quota of 60 hours. So please log out of the shell when not in use by entering 'exit'

### Get the exercise guides ###

All the files referred to in the exercise guides are available in a git repository hosted at [https://gitlab.com/cna-masterclass/masterclass.git](https://gitlab.com/cna-masterclass/masterclass.git). Once you have logged into the Google Cloud Shell you can clone this repository to follow a long the exercise guides

### Connect to the Kubernetes Cluster ###

Follow instructions from [connect-to-k8s.md](connect-to-k8s.md) to connect to the kubernetes cluster

### Get the exercise guides ###

All the files referred to in the exercise guides are available in a git repository hosted at [https://gitlab.com/cna-masterclass/masterclass.git](https://gitlab.com/cna-masterclass/masterclass.git). Once you have logged into the Google Cloud Shell you can clone this repository to follow along the exercise guides

``` sh
git clone https://gitlab.com/cna-masterclass/masterclass.git
```

All instructions assume that your present working directory will be `masterclass` cloned from the above command.

### Connect to Kubernetes ###

Follow instructions from [Connect to Kubernetes](connect-to-k8s.md) connect to the kubernetes cluster

## [Exercise 01](exercise-01.md) ##

Basic docker build and run

## [Exercise 02](exercise-02.md) ##

In this exercise we will deploy a simple web server.

>Introduces concepts of [deploymnent][deployment], [replicaset][replicaset], [pod][pod], [config-map][config-map] & [volumes][volume]

## [Exercise 03](exercise-03.md) ##

In this exercise we will publicly expose the simple web server

>Introduces concepts of [service][service], endpoints & [ingress][ingress]

## [Exercise 04](exercise-04.md) ##

In this exercise we will explore path based routing on the ingress

## [Extra](extra.md) ##

If you are finished with all the exercises then you can attempt some extra exercises



[deployment]: https://kubernetes.io/docs/concepts/workloads/controllers/deployment/
[replicaset]: https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/
[pod]: https://kubernetes.io/docs/concepts/workloads/pods/pod-overview/
[config-map]: https://kubernetes.io/docs/tasks/configure-pod-container/configure-pod-configmap/
[volume]: https://kubernetes.io/docs/concepts/storage/volumes/
[service]: https://kubernetes.io/docs/concepts/services-networking/service/
[ingress]: https://kubernetes.io/docs/concepts/services-networking/ingress/
